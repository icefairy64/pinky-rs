use std::{cell::RefCell, rc::Rc};

pub fn swampi_delay(ms: u32) {
    std::thread::sleep(std::time::Duration::from_millis(ms.into()));
}

// GPIO stuff

pub struct Pin<G: opi_gpio_lib::gpio::Gpio> {
    gpio: Rc<RefCell<G>>,
    index: i32,
    mode: opi_gpio_lib::gpio::PinMode,
    pull_up_down_mode: opi_gpio_lib::gpio::PinPullUpDown
}

impl <G: opi_gpio_lib::gpio::Gpio> Pin<G> {
    pub fn set_mode(&mut self, mode: opi_gpio_lib::gpio::PinMode) -> Result<(), Error> {
        self.mode = mode;
        Ok(self.gpio.borrow_mut().set_gpio_mode_raw(self.index, mode)?)
    }

    pub fn set_pull_up_down_mode(&mut self, mode: opi_gpio_lib::gpio::PinPullUpDown) -> Result<(), Error> {
        self.pull_up_down_mode = mode;
        Ok(self.gpio.borrow_mut().set_pull_up_down_control_raw(self.index, mode)?)
    }

    pub fn read_digital(&self) -> Result<bool, Error> {
        Ok(self.gpio.borrow_mut().digital_read_raw(self.index)?)
    }

    pub fn write_digital(&mut self, value: bool) -> Result<(), Error> {
        self.gpio.borrow_mut().digital_write_raw(self.index, value).map_err(Error::from)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Error {
    GpioError(opi_gpio_lib::gpio::Error),
    SpiError(opi_gpio_lib::spi::Error)
}

impl From<opi_gpio_lib::gpio::Error> for Error {
    fn from(value: opi_gpio_lib::gpio::Error) -> Self {
        <Self>::GpioError(value)
    }
}

impl From<opi_gpio_lib::spi::Error> for Error {
    fn from(value: opi_gpio_lib::spi::Error) -> Self {
        <Self>::SpiError(value)
    }
}

pub struct Swampi<G: opi_gpio_lib::gpio::Gpio> {
    gpio: Rc<RefCell<G>>
}

impl <G: opi_gpio_lib::gpio::Gpio> Swampi<G> {
    pub fn new(gpio: G) -> Self {
        Self {
            gpio: Rc::from(RefCell::from(gpio))
        }
    }

    pub fn create_pin(&mut self, pin: opi_gpio_lib::gpio::Pin, mode: opi_gpio_lib::gpio::PinMode) -> Result<Pin<G>, Error> {
        let mut pin = Pin {
            gpio: self.gpio.clone(),
            index: <G>::normalize_pin(pin).ok_or(Error::GpioError(opi_gpio_lib::gpio::Error::InvalidPin))?,
            mode,
            pull_up_down_mode: opi_gpio_lib::gpio::PinPullUpDown::Off
        };
        pin.set_mode(mode)?;
        Ok(pin)
    }
}