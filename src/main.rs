use std::{io::Read, env::args, time::SystemTime};

use image::{io::Reader, math::Rect};
use rusttype::Font;
use std::fs::File;

use crate::{eink::EinkDisplay, canvas::Canvas};

pub mod swampi;
pub mod eink;
pub mod canvas;
mod spi;

fn load_font(path: &str) -> Result<Font, String> {
    let mut file = File::open(path).map_err(|_| "Failed to open")?;
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).map_err(|_| "Failed to read")?;
    Font::try_from_vec(buffer).ok_or("Failed to load".to_string())
}

fn pad_rect(rect: &Rect, padding: u32) -> Rect {
    Rect {
        x: rect.x - padding,
        y: rect.y - padding,
        width: rect.width + padding * 2,
        height: rect.height + padding * 2,
    }
}

fn display_image() {
    let a: Vec<_> = args().collect();

    let mut canvas = Canvas::new(360, 240);
    let img = Reader::open(&a[1]).unwrap().decode().unwrap();
    let font = load_font("/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf").unwrap();

    let gpio = opi_gpio_lib::create_opi5_gpio().unwrap();
    let spi = crate::spi::SpiLibImpl::open("/dev/spidev4.1", 4_000_000).unwrap();
    let mut swampi = swampi::Swampi::new(gpio);

    let mut now = SystemTime::now();

    let mut busy_pin = swampi.create_pin(opi_gpio_lib::gpio::Pin::Physical(16), opi_gpio_lib::gpio::PinMode::Input).unwrap();
    busy_pin.set_pull_up_down_mode(opi_gpio_lib::gpio::PinPullUpDown::Up).unwrap();

    println!("setup {:?}us", SystemTime::now().duration_since(now).unwrap().as_micros());
    now = SystemTime::now();

    /*let mut buffer = [0x0u8; 10800];

    for x in 0..30usize {
        for y in 0..360usize {
            let mut byte = 0u8;
            for bt in 0..8 {
                byte = byte.shl(1);
                let adr = x * 8 + bt;
                if adr >= rand::random::<u8>().into() {
                    byte = byte | 0x01;
                }
            }
            buffer[x + (y * 30)] = byte;
        }
    }*/

    let mut display = EinkDisplay {
        spi,
        busy_pin,
        reset_pin: swampi.create_pin(opi_gpio_lib::gpio::Pin::Physical(15),opi_gpio_lib::gpio::PinMode::Output).unwrap(),
        cs_pin: swampi.create_pin(opi_gpio_lib::gpio::Pin::Physical(13),opi_gpio_lib::gpio::PinMode::Output).unwrap(),
        dc_pin: swampi.create_pin(opi_gpio_lib::gpio::Pin::Physical(11),opi_gpio_lib::gpio::PinMode::Output).unwrap(),
        width: 240,
        height: 360,
        lut_flag: false
    };

    display.init().unwrap();

    canvas.render_image_scaled(img, Rect { x: 0, y: 0, width: 360, height: 240 }).unwrap();

    println!("image render {:?}us", SystemTime::now().duration_since(now).unwrap().as_micros());
    now = SystemTime::now();

    /*for i in 0..16u8 {
        let x = (i as u32) * 22;
        let r = Rect {
            x: x,
            y: 0,
            width: 22,
            height: 240
        };
        canvas.fill_rect(r, i * 16, 0xff);
    }*/

    let text = &a[2];

    let text_box = canvas.layout_text(24, 24, text, &font, 24).unwrap();
    let fill_rect = pad_rect(&text_box, 12);

    canvas.fill_rect(fill_rect, 0x20, 0xa0).unwrap();
    canvas.render_border(fill_rect, 4, 0xff, 0xe0).unwrap();
    canvas.render_text(24, 24, text, &font, 24, 0xff).unwrap();

    println!("overlay render {:?}us", SystemTime::now().duration_since(now).unwrap().as_micros());
    now = SystemTime::now();
    
    let image_buffer = canvas.to_eink_vec_dithered().unwrap();

    println!("dither {:?}us", SystemTime::now().duration_since(now).unwrap().as_micros());
    now = SystemTime::now();
    
    display.display(&image_buffer).unwrap();

    println!("display {:?}us", SystemTime::now().duration_since(now).unwrap().as_micros());
    now = SystemTime::now();

    display.refresh().unwrap();
    println!("refresh {:?}us", SystemTime::now().duration_since(now).unwrap().as_micros());

    display.sleep().unwrap();
}

fn main() {
    display_image();
}
