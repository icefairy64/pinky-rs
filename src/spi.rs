pub struct SpiLibImpl {
    spidev: spidev::Spidev
}

impl SpiLibImpl {
    pub fn open(path: &str, speed: u32) -> Result<Self, crate::swampi::Error> {
        let mut spidev = spidev::Spidev::open(path).map_err(|_| crate::swampi::Error::SpiError(opi_gpio_lib::spi::Error::IoError))?;
        spidev.configure(spidev::SpidevOptions::new().max_speed_hz(speed)).map_err(|_| crate::swampi::Error::SpiError(opi_gpio_lib::spi::Error::IoError))?;
        Ok(Self {
            spidev
        })
    }
}

impl opi_gpio_lib::spi::Channel for SpiLibImpl {
    fn rw_data(&mut self, data: &mut [u8]) -> Result<(), opi_gpio_lib::spi::Error> {
        let tx_buf = Vec::from(data as &[u8]);
        let mut transfer = spidev::SpidevTransfer::read_write(tx_buf.as_slice(), data);
        self.spidev.transfer(&mut transfer).map_err(|_| opi_gpio_lib::spi::Error::IoError)
    }

    fn write(&mut self, data: &[u8]) -> Result<(), opi_gpio_lib::spi::Error> {
        for chunk in data.chunks(4096) {
            let mut transfer = spidev::SpidevTransfer::write(chunk);
            self.spidev.transfer(&mut transfer).unwrap();
        }
        Ok(())
    }
}