use crate::swampi::*;

#[derive(Debug, Clone, Copy)]
pub enum EinkDisplayError {
    Generic(Error),
    SpiError(opi_gpio_lib::spi::Error),
    IncorrectSize
}

impl From<Error> for EinkDisplayError {
    fn from(value: Error) -> Self {
        EinkDisplayError::Generic(value)
    }
}

impl From<opi_gpio_lib::spi::Error> for EinkDisplayError {
    fn from(value: opi_gpio_lib::spi::Error) -> Self {
        EinkDisplayError::SpiError(value)
    }
}

pub struct EinkDisplay<G: opi_gpio_lib::gpio::Gpio, S: opi_gpio_lib::spi::Channel> {
    pub spi: S,
    pub cs_pin: Pin<G>,
    pub dc_pin: Pin<G>,
    pub reset_pin: Pin<G>,
    pub busy_pin: Pin<G>,
    pub width: u16,
    pub height: u16,
    pub lut_flag: bool
}

const R20_GC: [u8; 56] = [0x01, 0x0f, 0x0f, 0x0f, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

const R21_GC: [u8; 42] = [0x01, 0x4f, 0x8f, 0x0f, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

const R22_GC: [u8; 56] = [0x01, 0x0f, 0x8f, 0x0f, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

const R23_GC: [u8; 42] = [0x01, 0x4f, 0x8f, 0x4f, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

const R24_GC: [u8; 42] = [0x01, 0x4f, 0x8f, 0x4f, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

impl <G: opi_gpio_lib::gpio::Gpio, S: opi_gpio_lib::spi::Channel> EinkDisplay<G, S> {
    pub fn send_byte(&mut self, data: u8) -> Result<(), EinkDisplayError> {
        self.cs_pin.write_digital(false)?;
        self.spi.write(&[data])?;
        self.cs_pin.write_digital(true)?;
        Ok(())
    }

    pub fn send_command(&mut self, data: u8) -> Result<(), EinkDisplayError> {
        self.dc_pin.write_digital(false)?;
        self.send_byte(data)
    }

    pub fn send_data_byte(&mut self, data: u8) -> Result<(), EinkDisplayError> {
        self.dc_pin.write_digital(true)?;
        self.send_byte(data)
    }

    pub fn send_data(&mut self, data: &[u8]) -> Result<(), EinkDisplayError> {
        self.dc_pin.write_digital(true)?;
        self.cs_pin.write_digital(false)?;
        self.spi.write(data)?;
        self.cs_pin.write_digital(true)?;
        Ok(())
    }

    pub fn reset(&mut self) -> Result<(), EinkDisplayError> {
        self.reset_pin.write_digital(true)?;
        swampi_delay(20);
        self.reset_pin.write_digital(false)?;
        swampi_delay(2);
        self.reset_pin.write_digital(true)?;
        swampi_delay(20);
        Ok(())
    }

    pub fn wait_busy(&self) -> Result<(), EinkDisplayError> {
        //swampi_delay(1400);
        while !self.busy_pin.read_digital()? {
            swampi_delay(10)
        }
        swampi_delay(50);
        Ok(())
    }

    pub fn refresh(&mut self) -> Result<(), EinkDisplayError> {
        self.send_command(0x17)?;
        self.send_data_byte(0xa5)?;
        self.wait_busy()?;
        swampi_delay(200);
        Ok(())
    }

    pub fn turn_on(&mut self) -> Result<(), EinkDisplayError> {
        self.send_command(0x12)?;
        self.send_data_byte(0)?;
        self.wait_busy()?;

        self.send_command(0x02)?;
        self.send_data_byte(0)?;
        self.wait_busy()?;

        Ok(())
    }

    pub fn sleep(&mut self) -> Result<(), EinkDisplayError> {
        self.send_command(0x07)?;
        self.send_data_byte(0xa5)
    }

    pub fn init(&mut self) -> Result<(), EinkDisplayError> {
        self.reset()?;

        self.send_command(0x00)?;
        self.send_data(&[0xff, 0x01])?;

        self.send_command(0x01)?;
        self.send_data(&[0x03, 0x10, 0x3f, 0x3f, 0x03])?;

        self.send_command(0x06)?;
        self.send_data(&[0x37, 0x3d, 0x3d])?;

        self.send_command(0x60)?;
        self.send_data_byte(0x22)?;

        self.send_command(0x82)?;
        self.send_data_byte(0x07)?;

        self.send_command(0x30)?;
        self.send_data_byte(0x09)?;

        self.send_command(0xe3)?;
        self.send_data_byte(0x88)?;

        // Resolution
        self.send_command(0x61)?;
        self.send_data(&[0xf0, 0x01, 0x68])?;

        self.send_command(0x50)?;
        self.send_data_byte(0xb7)?;

        Ok(())
    }

    pub fn display(&mut self, data: &[u8]) -> Result<(), EinkDisplayError> {
        if data.len() != (self.width / 8 * self.height).into() {
            return Err(EinkDisplayError::IncorrectSize);
        }
        self.send_command(0x13)?;
        self.send_data(data)?;
        self.lut_gc()
    }

    pub fn clear(&mut self) -> Result<(), EinkDisplayError> {
        self.send_command(0x13)?;
        for _i in 0..((self.width as u32) * (self.height as u32) / 8) {
            self.send_data_byte(0x00)?;
        }
        self.lut_gc()
    }

    pub fn lut_gc(&mut self) -> Result<(), EinkDisplayError> {
        self.send_command(0x20)?;
        self.send_data(&R20_GC)?;

        self.send_command(0x21)?;
        self.send_data(&R21_GC)?;

        self.send_command(0x24)?;
        self.send_data(&R24_GC)?;

        if !self.lut_flag {
            self.send_command(0x22)?;
            self.send_data(&R22_GC)?;

            self.send_command(0x23)?;
            self.send_data(&R23_GC)?;
        } else {
            self.send_command(0x22)?;
            self.send_data(&R23_GC)?;

            self.send_command(0x23)?;
            self.send_data(&R22_GC)?;
        }

        self.lut_flag = !self.lut_flag;

        Ok(())
    }

    pub fn init_a(&mut self) -> Result<(), EinkDisplayError> {
        self.reset()?;

        self.send_command(0x66)?;
        self.send_data(&[0x49, 0x55, 0x13, 0x5d, 0x05, 0x10])?;

        self.send_command(0xb0)?;
        self.send_data_byte(0x00)?;

        self.send_command(0x01)?;
        self.send_data(&[0x4f, 0x00])?;

        self.send_command(0x00)?;
        self.send_data(&[0x4f, 0x6b])?;

        self.send_command(0x06)?;
        self.send_data(&[0xd7, 0xde, 0x12])?;

        self.send_command(0x61)?;
        self.send_data(&[0x00, 0xa8, 0x01, 0x90])?;

        self.send_command(0x50)?;
        self.send_data_byte(0x37)?;

        self.send_command(0x60)?;
        self.send_data(&[0x0c, 0x05])?;

        self.send_command(0xe3)?;
        self.send_data_byte(0xff)?;

        self.send_command(0x84)?;
        self.send_data_byte(0x00)?;

        Ok(())
    }
}