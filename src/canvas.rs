use std::ops::{Sub, Shl};

use image::{DynamicImage, math::Rect};
use rusttype::{Font, Scale, Point};

pub struct Canvas {
    pub width: u16,
    pub height: u16,
    buffer: Vec<u8>
}

impl Canvas {
    pub fn new(width: u16, height: u16) -> Self {
        Canvas {
            width,
            height,
            buffer: vec![0; width as usize * height as usize]
        }
    }

    pub fn render_image(&mut self, image: DynamicImage) -> Result<(), String> {
        if let Some(content) = image.to_luma8().get(0..(self.width as usize * self.height as usize)) {
            self.buffer.copy_from_slice(content);
            Ok(())
        } else {
            Err("Could not convert to grayscale".to_string())
        }
    }

    pub fn render_image_scaled(&mut self, image: DynamicImage, rect: Rect) -> Result<(), String> {
        if rect.width % 8 != 0 || rect.height % 8 != 0 {
            panic!("Rect dimensions should be divisible by 8")
        }

        let content = image.resize_exact(rect.width, rect.height, image::imageops::FilterType::Nearest).to_luma8();

        let image_row_chunks = content.chunks(rect.width as usize);
        let target_chunks = self.buffer.chunks_mut(self.width as usize);
        
        let rect_x: usize = rect.x as usize;
        let rect_x2: usize = (rect.x + rect.width) as usize;

        target_chunks
            .skip(rect.y as usize)
            .take(rect.height as usize)
            .zip(image_row_chunks)
            .for_each(|(row, src_row)| {
                if let Some(target_slice) = row.get_mut(rect_x..rect_x2) {
                    target_slice.copy_from_slice(src_row);
                }
            });
            
        Ok(())
    }

    pub fn fill_rect(&mut self, rect: Rect, color: u8, opacity: u8) -> Result<(), String> {
        let target_chunks = self.buffer.chunks_mut(self.width as usize);
        let opacity_u16 = opacity as u16;
        let color_premul = ((color as u16).wrapping_mul(opacity_u16)) >> 8;
        for row in target_chunks.skip(rect.y as usize).take(rect.height as usize) {
            for pixel in row.get_mut((rect.x as usize)..(rect.x + rect.width) as usize).map_or(Err("No row slice"), Ok)? {
                let source = ((*pixel) as u16).wrapping_mul(0xffu16 - opacity_u16) >> 8;
                *pixel = (source + color_premul) as u8;
            }
        }
        Ok(())
    }

    pub fn render_text(&mut self, x: u16, y: u16, text: &str, font: &Font, size: u8, color: u8) -> Result<(), String> {
        let scale = Scale::uniform(size.into());
        let v_metrics = font.v_metrics(scale);
        let glyphs = font.layout(text, Scale::uniform(size.into()), Point { x: x.into(), y: f32::from(y) + v_metrics.ascent });
        let color_u16 = color as u16;
        for glyph in glyphs {
            if let Some(bb) = glyph.pixel_bounding_box() {
                let x_offset = bb.min.x as u16;
                let y_offset = bb.min.y as u16;
                glyph.draw(|x, y, v| {
                    let pixel = self.get_pixel_ref_mut((x as u16) + x_offset, (y as u16) + y_offset).unwrap();
                    *pixel = Self::multiply((*pixel) as u16, color_u16, (v * 512.0).clamp(0.0, 255.0).floor() as u16);
                });
            }
        }
        Ok(())
    }

    pub fn layout_text(&self, x: u16, y: u16, text: &str, font: &Font, size: u8) -> Result<Rect, String> {
        let scale = Scale::uniform(size.into());
        let v_metrics = font.v_metrics(scale);
        let glyphs: Vec<_> = font.layout(text, Scale::uniform(size.into()), Point { x: x.into(), y: f32::from(y) + v_metrics.ascent })
            .collect();

        let x_min = glyphs.iter()
            .flat_map(|g| g.pixel_bounding_box().into_iter())
            .map(|b| b.min.x as u32)
            .min()
            .unwrap_or(x as u32);
        let x_max = glyphs.iter()
            .flat_map(|g| g.pixel_bounding_box().into_iter())
            .map(|b| b.max.x as u32)
            .max()
            .unwrap_or(x as u32);

        let y_min = glyphs.iter()
            .flat_map(|g| g.pixel_bounding_box().into_iter())
            .map(|b| b.min.y as u32)
            .min()
            .unwrap_or(y as u32);
        let y_max = glyphs.iter()
            .flat_map(|g| g.pixel_bounding_box().into_iter())
            .map(|b| b.max.y as u32)
            .max()
            .unwrap_or(y as u32);

        Ok(Rect {
            x: x_min,
            y: y as u32,
            width: x_max - x_min,
            height: y_max - y_min
        })
    }

    pub fn render_border(&mut self, rect: Rect, width: u8, color: u8, opacity: u8) -> Result<(), String> {
        self.horizontal_line(rect.x as u16, (rect.x as u16) + (rect.width) as u16, rect.y as u16, width, color, opacity);
        self.horizontal_line(rect.x as u16, (rect.x as u16) + (rect.width) as u16, (rect.y as u16) + (rect.height as u16) - (width as u16), width, color, opacity);
        self.vertical_line((rect.y as u16) + (width as u16), (rect.y as u16) + (rect.height as u16) - (width as u16), rect.x as u16, width, color, opacity);
        self.vertical_line((rect.y as u16) + (width as u16), (rect.y as u16) + (rect.height as u16) - (width as u16), (rect.x as u16) + (rect.width as u16) - (width as u16), width, color, opacity);
        Ok(())
    }

    pub fn horizontal_line(&mut self, x1: u16, x2: u16, y: u16, width: u8, color: u8, opacity: u8) {
        let color_u16 = color as u16;
        let opacity_u16 = opacity as u16;

        let target_chunks = self.buffer.chunks_mut(self.width as usize).skip(y as usize).take(width as usize);
        for row in target_chunks {
            for pixel in row.iter_mut().skip(x1 as usize).take((x2 - x1) as usize) {
                *pixel = Self::multiply((*pixel) as u16, color_u16, opacity_u16);
            }
        }
    }

    pub fn vertical_line(&mut self, y1: u16, y2: u16, x: u16, width: u8, color: u8, opacity: u8) {
        let color_u16 = color as u16;
        let opacity_u16 = opacity as u16;

        let target_chunks = self.buffer.chunks_mut(self.width as usize).skip(y1 as usize).take((y2 - y1) as usize);
        for row in target_chunks {
            for pixel in row.iter_mut().skip(x as usize).take(width as usize) {
                *pixel = Self::multiply((*pixel) as u16, color_u16, opacity_u16);
            }
        }
    }

    pub fn to_eink_vec(&self, threshold: u8) -> Result<Vec<u8>, String> {
        let mut vec = vec![0xf0; self.width as usize * self.height as usize / 8];
        let target_chunks = vec.chunks_mut((self.height as usize) / 8);
        
        for (x, target_col) in target_chunks.enumerate() {
            for y in 0..(self.height as usize / 8) {
                let mut byte = 0u8;
                for bt in 0..8 {
                    byte <<= 1;
                    if self.get_pixel(x as u16, (bt + y * 8) as u16) > threshold {
                        byte |= 0x01;
                    }
                }
                target_col[(self.height as usize) / 8 - 1 - y] = byte.reverse_bits();
            }
        }

        Ok(vec)
    }

    pub fn to_eink_vec_dithered(&self) -> Result<Vec<u8>, String> {
        let mut vec = vec![0xf0; self.width as usize * self.height as usize / 8];
        let target_chunks = vec.chunks_mut((self.height as usize) / 8);

        let mut dither_counter = 0u8;

        let max = *(self.buffer.iter().max().unwrap()) as u16;
        let min = *(self.buffer.iter().min().unwrap()) as u16;
        let range = max - min + 1;

        for (x, target_col) in target_chunks.enumerate() {
            for y in 0..(self.height as usize / 8) {
                let mut byte = 0u8;
                for bt in 0..8 {
                    byte <<= 1;
                    let val = ((self.get_pixel(x as u16, (bt + y * 8) as u16) as u16).sub(min).shl(8) / range) as u8;
                    let sum = val as u16 + dither_counter as u16;
                    if sum >= 0xff {
                        byte |= 0x01;
                    }
                    dither_counter = (sum & 0xff) as u8;
                }
                target_col[(self.height as usize) / 8 - 1 - y] = byte.reverse_bits();
            }
        }

        Ok(vec)
    }

    fn get_pixel(&self, x: u16, y: u16) -> u8 {
        self.buffer[(x as usize) + y as usize * (self.width as usize)]
    }

    fn get_pixel_ref_mut(&mut self, x: u16, y: u16) -> Option<&mut u8> {
        self.buffer.get_mut((x as usize) + y as usize * (self.width as usize))
    }

    fn multiply(source: u16, target: u16, opacity: u16) -> u8 {
        let pm_target = (target * opacity) >> 8;
        let pm_src = (source * (0xffu16 - opacity)) >> 8;
        (pm_target + pm_src) as u8
    }
}